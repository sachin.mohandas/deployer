import os
import mysql.connector
from flask import jsonify

def get_data(cpu, ram, latitude, longitude):
    host = os.environ.get('DB_HOST')
    user = os.environ.get('DB_USER')
    password = os.environ.get('DB_PASSWORD')

    mydb = mysql.connector.connect(host=host, user=user, password=password)
    mycursor = mydb.cursor()

    query = None
    if cpu is not None and ram is None:
        query = f"SELECT * FROM region.aws JOIN price.aws ON region.aws.CLOUD = price.aws.CLOUD AND region.aws.CLD_REGION = price.aws.CLD_REGION WHERE price.aws.CPU >= {cpu} ORDER BY price.aws.PRICE ASC LIMIT 5"
    elif cpu is None and ram is not None:
        query = f"SELECT * FROM region.aws JOIN price.aws ON region.aws.CLOUD = price.aws.CLOUD AND region.aws.CLD_REGION = price.aws.CLD_REGION WHERE price.aws.RAM >= {ram} ORDER BY price.aws.PRICE ASC LIMIT 5"
    else:
        query = f"SELECT *, (6371 * acos(cos(radians({latitude})) * cos(radians(region.aws.LATITUDE)) * cos(radians(region.aws.LONGITUDE) - radians({longitude})) + sin(radians({latitude})) * sin(radians(region.aws.LATITUDE)))) AS distance FROM region.aws JOIN price.aws ON region.aws.CLOUD = price.aws.CLOUD AND region.aws.CLD_REGION = price.aws.CLD_REGION WHERE price.aws.CPU >= {cpu} AND price.aws.RAM >= {ram} ORDER BY price.aws.PRICE ASC, distance ASC LIMIT 5"

    mycursor.execute(query)
    results = mycursor.fetchall()

    data = []
    for result in results:
        data.append({'CLOUD': result[0], 'CLD_SKU': result[7], 'LOCATION': result[2], 'CLD_REGION': result[1], 'LATITUDE': result[3], 'LONGITUDE': result[4], 'PRICE': result[12]})

    mycursor.close()
    mydb.close()

    return jsonify(data)
