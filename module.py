import geocoder
import requests
from flask import Flask, jsonify, request

from get_data import get_data

app = Flask(__name__)

@app.route('/nearest_cld_sku', methods=['POST'])
def get_nearest_cld_sku():
    data = request.json
    cpu = data.get('cpu')
    ram = data.get('ram')
    if not cpu:
        return jsonify({'error': 'Missing CPU parameter'}), 400
    ip_address = request.headers.get('X-Forwarded-For', request.remote_addr)
    if ip_address.startswith('192.168.') or ip_address.startswith('10.') or ip_address.startswith('172.16.'):
        # Retrieve the public IP address of the client
        response = requests.get('https://api.ipify.org?format=json')
        ip_address = response.json()['ip']
    location = geocoder.ip(ip_address)
    if not location.latlng:
        return jsonify({'error': 'Unable to determine latitude and longitude from IP address.'}), 500
    latitude, longitude = location.latlng
    return get_data(cpu, ram, latitude, longitude)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)
